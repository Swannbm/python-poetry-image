
FROM python:3.9.14-slim-bullseye

# Tell apt-get we're never going to be able to give manual
ENV DEBIAN_FRONTEND=noninteractive

ENV POETRY_VERSION=1.2.0

# Update the package listing, so we know what package exist:
RUN apt update

# Install security updates:
RUN apt -y upgrade

RUN pip install --upgrade pip

RUN pip install "poetry==$POETRY_VERSION"

RUN poetry config virtualenvs.create false
RUN poetry config virtualenvs.in-project true

CMD [ "bin/sh" ]
